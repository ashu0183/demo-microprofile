package com.aro.absa.demo.microprofile;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class DemomicroprofileRestApplication extends Application {
}
